Write-Host -ForegroundColor Green "Installartion von Chocolatey"
Start-Sleep -Seconds 1

if (Test-Path C:\ProgramData\chocolatey\choco.exe) {
    Write-Host -ForegroundColor Yellow "Chocolatey ist installiert"
}
else {
    Write-Host -ForegroundColor Yellow "Chocolatey wird installiert, Bitte warten"
    #Ausgabe muss noch Unterdrueckt werden.
    Set-ExecutionPolicy Bypass -Scope Process -Force; `
    iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))  

    if (Test-Path C:\ProgramData\chocolatey\choco.exe) {
        Write-Host -ForegroundColor Green "Chocolatey wurde erfolgreich installiert"  
    }
    else {
        Write-Host -ForegroundColor Red "Chocolatey konnte nicht installiert werden."
    }
}

Write-Host -ForegroundColor Green "Es werden folgende Paktet installiert"

$Pakete = 'firefox', 'googlechrome', 'vlc', '7zip' , 'thunderbird' , 'adobereader' , 'libreoffice'

ForEach ($Paketname in $Pakete)
{
    choco install $Paketname -y
}